# 1. How to Learn Faster with the Feynman Technique

According to Feynman the best way to learn any concept is by explaining a concept in any area. If a concept is not clear, this technique is very useful to clarify the concept. This technique tests your level of understanding of a concept.

There are Four Steps involved in this technique:
##### Step 1:
- Choose a concept that you want to learn.
- Write the concept name at the top of the paper.

##### Step 2:
- Explain the concepts with simple language to the children.
- Use practical examples while explaining the concept.

##### Step 3:
- Identify the problems or points where you are stuck while explaining the concepts.
- Go back to the resources and clarify your doubts.

##### Step4:
- After clarification again explain the topic to someone.
- Keep an eye on your explanation.
- Note down their doubts and questions.
- Try to work on those doubts and questions until you are happy with your understanding


#### Features
1. Helps to quickly understand the concepts.
2. Indicate where your knowledge is solid and where you are poor.

# 2. What are the different ways to implement this technique in your learning process?

- Practice this technique with friends and colleagues.
- Practice this technique by mirror practice method.

# 3. How to Learn any thing new ?

This video tells us all about how to change our brains and learn new things.
Our brain is enormously complex. we can simplify its operation into two fundamental modes.
##### 1. Focus Mode:
 - Turning our focus attention to something.
##### 2. Diffuse mode:
- It's a relaxed set of neural states.

While we are learning we should switch between these two modes.

 ''Pinball game analogy helps us to understand these modes.''
- When we are learning we have to switch between focus mode and diffusion mode.
- whenever we try to solve a problem and we are stuck we have to leave that problem for some time and allow the diffuse mode of our mind.
- This means changing our thoughts to other topics which gives relaxation to our brains.
- After some time change our brain to focus mode and continue problem-solving
with new ideas which we got in diffuse mode.

#### What is the Pomodoro Technique?
Whenever we have to solve a new problem or task we tend to postpone that problem which causes a big problem in our life.
If we have a procrastination problem we have to follow the Pomodoro technique
###### This technique includes the following points 

- You need a timer.
- Just away from all the distractions and set 25 minutes in the timer.
- Work those 25 minutes with full focus and attention.
- After time up do something that gives relaxation to your brain for a little bit of time.
- Repeat the same steps to complete the task.

Using this method, we are habituating our brain to focus attention and also practicing our brain to relax a little bit instead of for a long time.

Finally, the relaxation process is also more important in our learning process.
#### Myths in Learning Process
- Some students feel that they are having a poor memory by comparing others
but that is a myth only. means they are storing other ideas or problems instead of the required ones. It's not a sign of poor memory.
- Some students think they are slow thinkers but that is not a problem,
in that process, they find more information along with the solution. but they should work hard to compete with others.

### Conclusion
##### To learn most effectively 

Understanding the topics by following points will master you in that topic.
1. Do exercises on the topics.
2. Practice as much as tests. 
3. Remember the topics as pictures and recall them frequently.

# 4. What are some of the steps that you can take to improve your learning process?


I can take the following steps to improve my learning process
- I pay full attention and focus whenever I am learning new concepts.
- Take breaks frequently for a short time to relax my mind.
- I will follow Fenyn Technique to test my level of understanding.
- Do exercises on topics and revise those frequently.


# 5 .Your key takeaways from the video? Paraphrase your understanding.

You can learn any new skill by investing 20 hours time with more focus. That means 45 minutes a day for a month.

Following are the steps to learn any skill in 20 hours.
#### Step1: Deconstruct the topic
- Decide what you want to do with that skill after learning.
- Analyse that skill and break it into smaller pieces.
- Select the parts of the skill that helps you and start practicing on those pieces.
- By practicing the most important things first, we will be able to improve our performance.

#### Step2: Learn enough to self-correct
- Collect  3 to 5 resources on what you want to learn.
-start practicing when you learn just enough this will leads to self-correction of your learning.
- By noticing the mistakes and correcting them we become better at that skill.

#### Step3: Remove practice barriers
- Avoid the things that distract you while learning.
- Feel confident while learning new things.

#### Step 4: Practice at least 20 hours
- overcome the initial frustration barrier.
- stick to consistent practice. 

The major barrier to learning is not intellectual it's emotional scarceness.

# 6.What are some of the steps that you can while approaching a new topic?


While approaching a new topic I follow the below steps
- Collect the resources that help to learn.
- Start reading basic stuff to understand that new topic.
- Look into the topics and select topics which are helpful to understand that total topic.
- Start practicing with the trial and error method and refer to the resources if you are stuck any where.
- Recall the old stuff before starting new concepts.
- Practice regularly to under the topic.






