# Grit and Growth Mindset
### 1. Paraphrase (summarize) the video in a few lines. Use your own words.
- The video talks about the importance of Grit and a Growth Mindset.
- Grit is a passion and perseverance for very long-term goals.
- With a Growth mindset we can build Grit in students.
- According to her one of the predictor of success is grit.

### 2. What are your key takeaways from the video to take action on?
Having only IQ is not enough, Grit and a Growth Mindset are very helpfull to get success in life. so focus on improving the Growth Mindset.

# 2. Introduction to Growth Mindset
### 3. Paraphrase (summarize) the video in a few lines in your own words.
Gives information about the fixed mindset and Growth mindset and Compares the fixed mindset and Growth mindset.
- Fixed Mindset peoples believe that skills are born and can not learn and grow.
- Growth Mindset people believe skills are built so that can learn.

Talks about the methods to improve a Growth Mindset.

### 4. Some of the key takeaways from the video are 
- A growth Mindset is very important and powerful to learn.
- Is the foundation for learning.
- Growth Mindset increased by keeping efforts and learning from mistakes.
- By having a Growth Mindset, we can achieve more goals in our life.

# 3. Understanding Internal Locus of Control.
### 5. What is the Internal Locus of Control? What is the key point in the video?
It talks about the internal locus of control and external locus of control.
Locus of control is the degree to which you believe you have control over your life.
Internal locus of control is how much effort you put into something that you have complete control over it. to adapt internal locus of control all the time, feel motivated all the time.

# 3. How to build a Growth Mindset.
## 6. Paraphrase (summarize) the video in a few lines in your own words.
It further talks about the Fixed and  Growth Mindset and how it is helpful to control your life. It talks about how to develop a Growth Mindset. Which can be developed by believing in ourselves and talks about the inability to do something and how to overcome it.
## 7. What are your key takeaways from the video to take action on?
key takeaways from the video are
- Believe in your ability to figure things out.
- Question your assumptions.
- Prepare your curriculum for growth.
- Motivate yourself.

# 4. Mindset - A MountBlue Warrior Reference Manual
### 8. What are one or more points that you want to take action on from the manual? (Maximum 3)
-   I know more efforts lead to better understanding.
-   I will follow the steps required to solve problems:
    - Relax
    - Focus - What is the problem? What do I need to know to solve it?
    - Understand - Documentation, Google, Stack Overflow, Github Issues, Internet
    - Code
    - Repeat
-I will use the weapons of Documentation, Google, Stack Overflow, GitHub Issues, and the Internet before    asking for help from fellow warriors or look at the solution.


