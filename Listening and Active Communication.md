# 1. What are the steps/strategies to do Active Listening? 
- Listening to someone and comprehending the meaning of that listening.
- While listening avoids the distractions that distract you while listening.
- Focus on the speaker and topic.
- Do not interrupt the other person while speaking let them complete and respond.
- Use door openers while listening these door openers indicates that you are actively listening to them.
- Show that you are listening to the speaker appropriate body language and maintaining eye contact.
- Takes notes or key points on the topic while listening.

# 2. According to Fisher's model, what are the key points of Reflective Listening? 
- keep distractions away and focus on communication.
- It let the speaker know the understanding properly to the listener.
- It requires responding actively to another by keeping your attention completely on the speaker.
- Listener sholud try to mirror the mood of speaker.
- Reponds to the speaker specific points with out distracting him to another topic.

# 3. What are the obstacles in your listening process?
- Get distracted by mobile and own thoughts.

# 4. What can you do to improve your listening?
- I will try to avoid distractions like using mobile while learning.
- I will try to compose notes after the completion of listening.

# 5. When do you switch to a Passive communication style in your day-to-day life?
- When relations or friends ask for something.

# 6. When do you switch in to Aggressive communication styles in your day to day life?
- Most of the time I won't switch to aggressive communication

# 7.When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- When I am with my friends.

# 8.How can you make your communication assertive?

- I will calm down and I am in under control before starting to speak.
- Prepare a draft on the topic and verify once or twice.
- Practice prepared drafts and rectifying the mistakes before starting a communication.
- Speak out what needs directly and respectfully.
- Don't control other's communication.
- Respects others points of view.
- Once I complete my communication I will listen to others with patience.
