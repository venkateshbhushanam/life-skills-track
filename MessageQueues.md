# What is the Message? 
A message is the data transported between the sender and the receiver application. It's in the form of a byte array with some headers at the top. An example of a message could be something that tells one system to start processing a task, it may contain a plain message.
# What is Queue?
A queue is a line of things waiting to be handled, starting at the beginning of the line and processing it in sequential order. 
# What is Asynchronous Communication?
The sender or client application sends the message to the queue but it will not bother the consumer response. The best example of asynchronous communication is sending an email. When an email is sent, the sender continues to process other things without needing an immediate response from the receiver. 
# What is Message Queue?
A message queue is a queue of messages sent between applications. It includes a sequence of work objects that are waiting to be processed. message queue works on asynchronous communication. Message queuing allows applications to communicate by sending messages to each other. The message queue provides temporary message storage when the destination program is busy or not connected.

`Examples of queues`: Kafka, Heron, real-time streaming, Amazon SQS, and RabbitMQ
# The basic architecture of a message queue
[![N|Solid](https://i0.wp.com/blog.codepath.com/wp-content/uploads/2012/11/mq_illustration_21.png?resize=390%2C506)](https://nodesource.com/products/nsolid)
There are client applications called producers that create messages and deliver them to the message queue instead of delivering them directly to the consumer. The consumer application means which uses messages connect to the queue and gets the messages to be processed. Messages placed in the queue are stored until the consumer retrieves them. Here broker acts as a monitor and assigns messages to the consumer.
# Example for Messaging queues — RabbitMQ:
[![N|Solid](https://miro.medium.com/max/720/0*xHZPA79AzJ_3tyWP)](https://nodesource.com/products/nsolid)
RabbitMQ is one of the most widely used message brokers, it acts as the message broker
RabbitMQ consists of:
1. producer — the client that creates a message
2. consumer — receives a message
3. queue — stores messages
3. exchange — enables the route of messages and sending them to queues

Functioning:
1. producer creates a message and sends it to an exchange
2. exchange receives a message and routes it to queues subscribed to it
3. consumer receives messages from those queues he/she is subscribed to
One should note that messages are filtered and routed depending on the type of exchange.

# Why we are using it?
MCQs provide persisting data until it has been fully processed. The put-get-delete paradigm, which many message queues use, requires a process to explicitly indicate that it has finished processing a message before the message is removed from the queue, ensuring your data is kept safe until you’re done with it.
In message queuing the consumers are continuously monitored by one mechanism which may call Notifier and if in any condition the consumer fails to consume the messages, those messages will be allotted to another consumer.
Message queues enable asynchronous processing, which allows you to put a message in the queue without processing it immediately. it processes all the messages on priority bases.
Message queues are very useful when we want to do a Batching, Instead of giving one message at a time, we can give more number of message to the queues at a time. this will help to optimize the performance.
# Top Message Queues Tools
- IBM MQ.
- MuleSoft Anypoint Platform.
- Apache Kafka.
- Apache Qpid.
- Nastel.
- IBM Cloud Pak for Integration.
- Azure Scheduler.
- RabbitMQ.

# What is Enterprise Message Bus?
Enterprise Message Bus is used to reduce the application integrity complexity in an enterprise where more than two applications need to be integrated.
Enterprise Service bus or message Bus uses message transformers to convert the message to the format expected by the receiving application.if the application is down it saves the message and sends that when the application is available ensuring that there is no data loss
provides the routing and data transmission service
ESB also provides security by authenticating the applications before sending the message
provides a logging service to track the message routing as well.
# References 
* https://www.cloudamqp.com/blog/what-is-message-queuing.html
* http://blog.codepath.com/2013/01/06/asynchronous-processing-in-web-applications-part-2-developers-need-to-understand-message-queues/
* https://medium.com/must-know-computer-science/system-design-message-queues-245612428a22
* https://youtu.be/eVrgMZH2jNY



 
